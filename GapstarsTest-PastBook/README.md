# PastBook E2E Automation Project

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)]()

This framework has been developed using Cucumber BDD, JUnit and Selinium It includes the junit cucumber html reporter as well.  

# How to execute
##### Method-01
  - Open from InteliJ IDEA or Eclipse.
  - Navigate To /src/test/java/cucumberOptions/TestRunner.java
  - Righ-click on the file and choose Run "TestRunner"
 
##### Method-02
- Open from InteliJ IDEA or Eclipse.
- Right-click on the project folder.
- Choose Run and then "All Tests" or Select the Specific Feature needs to execute.

## Usage

### Gherkin Feature Sample
```gherkin
Background:
    Given I am in Pastbook Home Page

  Scenario Outline: As a user I should be able to Sign up and reset Password
    When I click on sign in menu
    And I navigated to sign in popup screen
    And I click on use email option
    And I enter the <email> to the email text box
    And I click on submit button
    And I get re-directed to Home Page as a Logged in User
    And I navigated to profile
    And I reset the password to <password>
    Then I click on save password button

    Examples:
    |email|password|
    |test+123@gmail.com|123@com|
```

### Step Definition Sample

```java
@When("^I get navigated to create Past Book Page$")
    public void i_get_navigated_to_create_book_page(){
        createPastBookPageObjects = new CreatePastBookPageObjects(driver);

        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(createPastBookPageObjects.waitTitle()));
    }
    @When("^I enter (.*) into Title text field$")
    public void i_enter_title_into_field(String title){
        createPastBookPageObjects.getTitleTxt().sendKeys(title);
    }
    @When("^I click on option link text$")
    public void i_click_on_option_link_text(){
        createPastBookPageObjects.getOptionLinkTxt().click();

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(createPastBookPageObjects.waitAddCover()));
    }
    @When("^I click on Add Cover Button$")
    public void i_click_on_add_cover_button(){
        createPastBookPageObjects.getAddCoverBtn().click();

        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(createPastBookPageObjects.waitDragNDrop()));
    }
```

### Page Object Class Sample
```java
public class HomePageObjects {
    WebDriver driver;

    public HomePageObjects(WebDriver driver){
        this.driver=driver;
    }

    By signInMenu = By.id("nav-item-signin");
    By createMenuItem = By.xpath("//*[@id='nav-item-create']/a");

    public WebElement getSignInMenu (){
        return driver.findElement(signInMenu);
    }
    public WebElement getCreateMenuItem(){
        return driver.findElement(createMenuItem);
    }

}
```