$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("01-Registration.feature");
formatter.feature({
  "line": 1,
  "name": "As a Pastbook User I should be able to sign up to the application by providing email and I should be able to reset the password",
  "description": "",
  "id": "as-a-pastbook-user-i-should-be-able-to-sign-up-to-the-application-by-providing-email-and-i-should-be-able-to-reset-the-password",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 6,
  "name": "As a user I should be able to Sign up and reset Password",
  "description": "",
  "id": "as-a-pastbook-user-i-should-be-able-to-sign-up-to-the-application-by-providing-email-and-i-should-be-able-to-reset-the-password;as-a-user-i-should-be-able-to-sign-up-and-reset-password",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "I click on sign in menu",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I navigated to sign in popup screen",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on use email option",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I enter the \u003cemail\u003e to the email text box",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I click on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I get re-directed to Home Page as a Logged in User",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I navigated to profile",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I reset the password to \u003cpassword\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I click on save password button",
  "keyword": "Then "
});
formatter.examples({
  "line": 17,
  "name": "",
  "description": "",
  "id": "as-a-pastbook-user-i-should-be-able-to-sign-up-to-the-application-by-providing-email-and-i-should-be-able-to-reset-the-password;as-a-user-i-should-be-able-to-sign-up-and-reset-password;",
  "rows": [
    {
      "cells": [
        "email",
        "password"
      ],
      "line": 18,
      "id": "as-a-pastbook-user-i-should-be-able-to-sign-up-to-the-application-by-providing-email-and-i-should-be-able-to-reset-the-password;as-a-user-i-should-be-able-to-sign-up-and-reset-password;;1"
    },
    {
      "cells": [
        "testpastbook+14@testemail.com",
        "123@com"
      ],
      "line": 19,
      "id": "as-a-pastbook-user-i-should-be-able-to-sign-up-to-the-application-by-providing-email-and-i-should-be-able-to-reset-the-password;as-a-user-i-should-be-able-to-sign-up-and-reset-password;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I am in Pastbook Home Page",
  "keyword": "Given "
});
formatter.match({
  "location": "CommonSteps.i_am_in_Pastbook_Home_Page()"
});
formatter.result({
  "duration": 9097759900,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "As a user I should be able to Sign up and reset Password",
  "description": "",
  "id": "as-a-pastbook-user-i-should-be-able-to-sign-up-to-the-application-by-providing-email-and-i-should-be-able-to-reset-the-password;as-a-user-i-should-be-able-to-sign-up-and-reset-password;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "I click on sign in menu",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I navigated to sign in popup screen",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on use email option",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I enter the testpastbook+14@testemail.com to the email text box",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I click on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I get re-directed to Home Page as a Logged in User",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I navigated to profile",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I reset the password to 123@com",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I click on save password button",
  "keyword": "Then "
});
formatter.match({
  "location": "CommonSteps.i_click_on_sign_in_menu()"
});
formatter.result({
  "duration": 253701200,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_navigated_to_sign_in_popup_screen()"
});
formatter.result({
  "duration": 5454701800,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_use_email_option()"
});
formatter.result({
  "duration": 111614100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "testpastbook+14@testemail.com",
      "offset": 12
    }
  ],
  "location": "CommonSteps.i_enter_the_test_email_com_to_the_email_text_box(String)"
});
formatter.result({
  "duration": 257188400,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_submit_button()"
});
formatter.result({
  "duration": 111676200,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_get_re_directed_to_Home_Page_as_a_Logged_in_User()"
});
formatter.result({
  "duration": 6496617000,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.iNavigatedToProfile()"
});
formatter.result({
  "duration": 1267124100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123@com",
      "offset": 24
    }
  ],
  "location": "CommonSteps.iResetThePasswordToPassword(String)"
});
formatter.result({
  "duration": 3749825100,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.iClickOnSavePasswordButton()"
});
formatter.result({
  "duration": 1401562100,
  "status": "passed"
});
formatter.uri("02-CreatePastBookAlbum.feature");
formatter.feature({
  "line": 1,
  "name": "As a User I should be able to Create a PastBook",
  "description": "",
  "id": "as-a-user-i-should-be-able-to-create-a-pastbook",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 6,
  "name": "As a Logged in User I should be able to create a PastBook",
  "description": "",
  "id": "as-a-user-i-should-be-able-to-create-a-pastbook;as-a-logged-in-user-i-should-be-able-to-create-a-pastbook",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "I click on sign in menu",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I navigated to sign in popup screen",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on use email option",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I enter the \u003cemail\u003e to the email text box",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I click on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I enter the \u003cpassword\u003e on the password text box",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I click on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I get re-directed to Home Page as a Logged in User",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I click on create menu option",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I get navigated to create Past Book Page",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I close pop-up advertisement",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I enter \u003ctitle\u003e into Title text field",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "I click on option link text",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "I click on Add Cover Button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I add a cover photo \u003cimg\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "I click on create your Pastbook button",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "I click on upload picture",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "I add album photo \u003cimg1\u003e and click on upload",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I click on continue button",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "I logout from pastbook application",
  "keyword": "And "
});
formatter.examples({
  "line": 28,
  "name": "",
  "description": "",
  "id": "as-a-user-i-should-be-able-to-create-a-pastbook;as-a-logged-in-user-i-should-be-able-to-create-a-pastbook;",
  "rows": [
    {
      "cells": [
        "email",
        "password",
        "title",
        "img",
        "img1"
      ],
      "line": 29,
      "id": "as-a-user-i-should-be-able-to-create-a-pastbook;as-a-logged-in-user-i-should-be-able-to-create-a-pastbook;;1"
    },
    {
      "cells": [
        "testpastbook+14@testemail.com",
        "123@com",
        "My Test Past Book",
        "7.jpg",
        "3.jpg"
      ],
      "line": 30,
      "id": "as-a-user-i-should-be-able-to-create-a-pastbook;as-a-logged-in-user-i-should-be-able-to-create-a-pastbook;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I am in Pastbook Home Page",
  "keyword": "Given "
});
formatter.match({
  "location": "CommonSteps.i_am_in_Pastbook_Home_Page()"
});
formatter.result({
  "duration": 6813315300,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "As a Logged in User I should be able to create a PastBook",
  "description": "",
  "id": "as-a-user-i-should-be-able-to-create-a-pastbook;as-a-logged-in-user-i-should-be-able-to-create-a-pastbook;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "I click on sign in menu",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I navigated to sign in popup screen",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click on use email option",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I enter the testpastbook+14@testemail.com to the email text box",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I click on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I enter the 123@com on the password text box",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I click on submit button",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I get re-directed to Home Page as a Logged in User",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I click on create menu option",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I get navigated to create Past Book Page",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I close pop-up advertisement",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I enter My Test Past Book into Title text field",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "I click on option link text",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "I click on Add Cover Button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I add a cover photo 7.jpg",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "I click on create your Pastbook button",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "I click on upload picture",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "I add album photo 3.jpg and click on upload",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I click on continue button",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "I logout from pastbook application",
  "keyword": "And "
});
formatter.match({
  "location": "CommonSteps.i_click_on_sign_in_menu()"
});
formatter.result({
  "duration": 187138900,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_navigated_to_sign_in_popup_screen()"
});
formatter.result({
  "duration": 5183126300,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_use_email_option()"
});
formatter.result({
  "duration": 119149300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "testpastbook+14@testemail.com",
      "offset": 12
    }
  ],
  "location": "CommonSteps.i_enter_the_test_email_com_to_the_email_text_box(String)"
});
formatter.result({
  "duration": 260519600,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_submit_button()"
});
formatter.result({
  "duration": 125210800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "123@com",
      "offset": 12
    }
  ],
  "location": "CommonSteps.i_enter_the_password_text_box(String)"
});
formatter.result({
  "duration": 1302930300,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_submit_button()"
});
formatter.result({
  "duration": 87349500,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_get_re_directed_to_Home_Page_as_a_Logged_in_User()"
});
formatter.result({
  "duration": 4339223200,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_create_menu_option()"
});
formatter.result({
  "duration": 101881100,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_get_navigated_to_create_book_page()"
});
formatter.result({
  "duration": 3038685100,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.iClosePopUpAdvertisement()"
});
formatter.result({
  "duration": 170877100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "My Test Past Book",
      "offset": 8
    }
  ],
  "location": "CommonSteps.i_enter_title_into_field(String)"
});
formatter.result({
  "duration": 244143200,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_option_link_text()"
});
formatter.result({
  "duration": 769131800,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_add_cover_button()"
});
formatter.result({
  "duration": 2900561500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "7.jpg",
      "offset": 20
    }
  ],
  "location": "CommonSteps.i_add_a_cover_photo(String)"
});
formatter.result({
  "duration": 10378062500,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.i_click_on_create_pastbook_button()"
});
formatter.result({
  "duration": 18743169700,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.iClickOnUploadPicture()"
});
formatter.result({
  "duration": 2911111000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "3.jpg",
      "offset": 18
    }
  ],
  "location": "CommonSteps.iAddAlbumPhotoImgAndImgAndClickOnUpload(String)"
});
formatter.result({
  "duration": 323399400,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.iClickOnContinueButton()"
});
formatter.result({
  "duration": 15116313000,
  "status": "passed"
});
formatter.match({
  "location": "CommonSteps.iSeePublishedBookPage()"
});
formatter.result({
  "duration": 4614381600,
  "status": "passed"
});
});