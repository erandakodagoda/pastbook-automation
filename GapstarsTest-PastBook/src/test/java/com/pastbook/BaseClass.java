package com.pastbook;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class BaseClass {
    public static WebDriver driver;
    public static Properties properties;

    public static WebDriver getDriver() throws IOException {
        //Loading Properties in the File
        properties = new Properties();
        String userdir = System.getProperty("user.dir");
        String propFilePath = "\\src\\test\\java\\com\\pastbook\\global.properties";
        FileInputStream fileInputStream = new FileInputStream(userdir+propFilePath);
        properties.load(fileInputStream);

        //Setting Up Chrome Driver
        String chromePath = properties.getProperty("chromedriver");
        System.setProperty("webdriver.chrome.driver",userdir+chromePath);
        driver = new ChromeDriver();

        driver.get(properties.getProperty("url"));
        return driver;
    }
    public static String getImagePath(String file){
        String filePath = System.getProperty("user.dir")+"\\images\\"+file;
        return filePath;
    }

    public void quitDriver(){
        driver.quit();
    }
}
