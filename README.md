# Pastbook Repository
In the Pastbook repository below things will be included

  - GapstarsTest-PastBook- (Automation Code) BDD (JUnit+Cucumber+Selenium+CucumberHTMLReport).
  - Screencasts - Automation Execution and Manual Execution
  - Test Plan - Test Plan documentation
  - Test Cases - Test Cases for Signup, Signin, Update Password and Create Pastbook scenarios.
